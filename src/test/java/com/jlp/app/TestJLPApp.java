package com.jlp.app;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class TestJLPApp {

	static WebDriver driver ;
	String expectedTitle;
	String actualTitle;

	@BeforeClass
	public static void setUp() {
		// code that will be invoked when this test is instantiated
		System.setProperty("webdriver.chrome.driver", "D:/Softwares/chromedriver.exe");
		driver = new ChromeDriver();
		String baseUrl = "http://localhost:8082/JLPApp/";
		driver.get(baseUrl);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testTitle() {
		try {

			expectedTitle = "Welcome";
			actualTitle = "";
			actualTitle = driver.getTitle();

			expectedTitle = "WelcomePartner";
			actualTitle = driver.getTitle();
			

			Assert.assertEquals(actualTitle, expectedTitle);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testLogin() {

		try {
			driver.findElement(By.linkText("Login")).click();
			expectedTitle = "Login";
			actualTitle = driver.getTitle();
			Assert.assertEquals(actualTitle, expectedTitle);
			driver.findElement(By.id("username")).clear();
			driver.findElement(By.id("username")).sendKeys("dahveed");
			Thread.sleep(2000);

			// Clear pass and enter password, click submit
			driver.findElement(By.id("password")).clear();
			driver.findElement(By.id("password")).sendKeys("123456");
			driver.findElement(By.id("login")).click();
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testWelcomePage() {
		expectedTitle = "WelcomePartner";
		actualTitle = driver.getTitle();
		Assert.assertEquals(actualTitle, expectedTitle);
	}

	@AfterClass
	public static void exit() {
		driver.close();
	}

}
